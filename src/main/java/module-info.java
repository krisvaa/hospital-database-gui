module no.ntnu.krisvaa.idatt2001.patientregister {
    requires javafx.controls;
    requires javafx.fxml;
    exports no.ntnu.krisvaa.idatt2001.patientregister.view;
    exports no.ntnu.krisvaa.idatt2001.patientregister.controllers;

    requires java.persistence;
    requires java.base;
    requires java.sql;
    requires net.bytebuddy;
    requires java.xml.bind;
    requires org.hibernate.orm.core;
    exports no.ntnu.krisvaa.idatt2001.patientregister.model;

    opens no.ntnu.krisvaa.idatt2001.patientregister.model to org.hibernate.orm.core;
}
