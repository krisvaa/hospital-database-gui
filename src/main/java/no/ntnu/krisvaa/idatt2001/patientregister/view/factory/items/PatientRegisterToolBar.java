package no.ntnu.krisvaa.idatt2001.patientregister.view.factory.items;

import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;

public class PatientRegisterToolBar extends ToolBar {

    private Button addPatient;
    private Button removePatient;
    private Button editPatient;

    public PatientRegisterToolBar() {
        addPatient = new Button("Add patient");
        removePatient = new Button("Remove patient");
        editPatient = new Button("Edit patient");

        this.getChildren().addAll(
                addPatient,
                removePatient,
                editPatient
        );
    }
}
