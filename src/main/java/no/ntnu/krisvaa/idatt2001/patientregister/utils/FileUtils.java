package no.ntnu.krisvaa.idatt2001.patientregister.utils;

import java.io.File;

public class FileUtils {

    /**
     * Checks if a file exists on disk or not.
     *
     * @param path Path of the file to check if exists or not
     * @return True if file exists, false if not
     */
    public static boolean fileExists(String path) {
        File f = new File(path);
        return f.exists();
    }

    /**
     * Warning: No confirmation is provided before deletion!
     *
     * Method to easily delete file.
     * @param filePath Filepath of file to be deleted
     * @return true if the file was deletes, false if something went wrong.
     */
    public static boolean deleteFile(String filePath) {
        File f = new File(filePath);
        return f.delete();
    }
}
