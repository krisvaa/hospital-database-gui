package no.ntnu.krisvaa.idatt2001.patientregister.view;

import javafx.scene.control.Alert;


/**
 * Dialog to show a confirm box to the user.
 */
public class ConfirmDialog extends Alert {

    private static final String CONFIRM_DIALOG_TITLE = "Patient Register - Confirm";

    public ConfirmDialog(String question) {
        super(AlertType.CONFIRMATION);
        setTitle(CONFIRM_DIALOG_TITLE);
        setContentText(question);
    }
}
