package no.ntnu.krisvaa.idatt2001.patientregister.model;

import no.ntnu.krisvaa.idatt2001.patientregister.constants.PatientField;

import static no.ntnu.krisvaa.idatt2001.patientregister.constants.Constants.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class to write patient data to a CSV-File
 */
public class PatientCSVFileWriter {

    private File FILE;
    private String delimiter = CSV_DELIMITER_STRING;

    public PatientCSVFileWriter(String path) throws IOException {
        if(!path.endsWith(".csv")) {
            throw new IOException(path + " is not a .csv file.");
        }

        this.FILE = new File(path);

        if(FILE.exists()) {
            throw new IOException(path + "already exist.");
        }
    }


    /**
     * Writes data to {@link #FILE}, splits the data into lines. And splits each line into values where delimiter ; is found.
     *
     * @return List containing lines from CSV file. And one line is represented a list of String values.
     * @throws IOException Problems reading from the file.
     */

    /**
     * Writes data to {@link #FILE}. Each line is represented with an ArrayList with another ArrayList inside itself.
     * The ArrayList inside is representation of values. Each line will be written with it's corresponding values, with ; between
     *
     * @param fileData List containing lines, with each line containing a list of values to be written to file.
     * @throws IOException Error reading to file
     * @throws IllegalArgumentException If a value contains delimiter ;.
     */
    protected void saveRawFileDataToFile(ArrayList<ArrayList<String>> fileData) throws IOException, IllegalArgumentException {
        boolean dataEmpty = fileData.isEmpty();

        //Verify data before opening file.
        for(ArrayList<String> lineData : fileData) {
            for(String data : lineData) {
                if(data.contains(delimiter)) {
                    throw new IllegalArgumentException(delimiter + " is used as delimiter in csv-file, and is not allowed in file data.");
                }
            }
        }

        try (PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(FILE)))){

            if(dataEmpty) {
                printWriter.write("");
                return;
            }

            for(ArrayList<String> lineData : fileData) {
                String line = "";

                for(String data : lineData) {
                    if(data.contains(delimiter)) {
                        throw new IllegalArgumentException(delimiter + " is used as delimiter in csv-file, and is not allowed in file data.");
                    }

                    line += (data + delimiter);
                }

                printWriter.println(line);
            }

        }
    }

    /**
     *  Method for entering a list of {@link Patient} to be saved to the CSV-File.
     *
     * First the list the header-line is added to a new ArrayList according to {@link no.ntnu.krisvaa.idatt2001.patientregister.constants.Constants#CSV_PATIENT_FIELD_HEADERS}.
     * Then each PatientObject has their values added to a String with the delimiter ; between, in the correct order according to the header-line.
     *
     * This is then sent to {@link #saveRawFileDataToFile(ArrayList)} to be saved in CSV format to the file.
     *
     * @param patients Patients to be saved to the CSV-File
     * @throws IOException If there is an error writing to the CSV-File
     */
    public void saveToFile(ArrayList<Patient> patients) throws IOException {
        ArrayList<ArrayList<String>> fileData = new ArrayList<>();

        String[] headers = CSV_PATIENT_FIELD_HEADERS.keySet().toArray(new String[CSV_PATIENT_FIELD_HEADERS.size()]);
        ArrayList<String> headerLine = new ArrayList<>(Arrays.asList(headers));

        fileData.add(headerLine);

        for (Patient p : patients) {
            ArrayList<String> lineData = new ArrayList<String>(headerLine.size());

            for(String header : headerLine) {
                PatientField field = CSV_PATIENT_FIELD_HEADERS.get(header);
                int columnIndex = headerLine.indexOf(header);

                String fieldData;

                switch (field) {
                    case GENERAL_PRACTITIONER -> fieldData = p.getGeneralPractitioner();
                    case FIRST_NAME -> fieldData = p.getFirstName();
                    case SURNAME -> fieldData = p.getSurname();
                    case DIAGNOSIS -> fieldData = p.getDiagnosis();
                    case SOCIAL_SECURITY_NUMBER -> fieldData = p.getSocialSecurityNumber();
                    default -> fieldData = "";
                };

                if(fieldData == null) {
                    fieldData = "";
                }

                lineData.add(columnIndex, fieldData);
            }

            fileData.add(lineData);

        }

        saveRawFileDataToFile(fileData);
    }

}
