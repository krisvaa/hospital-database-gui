package no.ntnu.krisvaa.idatt2001.patientregister.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import no.ntnu.krisvaa.idatt2001.patientregister.constants.Constants;
import no.ntnu.krisvaa.idatt2001.patientregister.exceptions.*;
import no.ntnu.krisvaa.idatt2001.patientregister.model.Patient;
import no.ntnu.krisvaa.idatt2001.patientregister.model.PatientCSVFileReader;
import no.ntnu.krisvaa.idatt2001.patientregister.model.PatientCSVFileWriter;
import no.ntnu.krisvaa.idatt2001.patientregister.model.PatientRegister;
import no.ntnu.krisvaa.idatt2001.patientregister.utils.FileUtils;
import no.ntnu.krisvaa.idatt2001.patientregister.view.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * Controller for the main frame. Connected with the scenes/main-frame.fxml document.
 *
 */
public class MainFrameController {

    private PatientRegister patientRegister;

    @FXML
    public TableView<Patient> patientTableView;
    @FXML
    public TableColumn<Patient, String> socialSecurityNumberColumn;
    @FXML
    public TableColumn<Patient, String> surnameColumn;
    @FXML
    public TableColumn<Patient, String> firstNameColumn;

    @FXML
    public Text statusText;

    public MainFrameController() {

    }

    /**
     * Method to initialize the controller.
     *
     * Creates a patient register, and initializes the tableview.
     *
     */
    @FXML
    public void initialize() {
        try {
            patientRegister = new PatientRegister(Constants.PERSISTENCE_UNIT_PROD);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorDialog errorDialog = new ErrorDialog("Unable to connect to database. Reason: " + e.getMessage());
            errorDialog.showAndWait();
        }

        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));

        patientTableView.getItems().addAll(patientRegister.getAllPatients());
    }

    /**
     *
     * Triggered by action defines in fxml-document.
     * Triggers CSV-import.
     *
     * Opens a file-picker for the user, so the user can select a .csv file.
     * The CSV file will then be read by a {@link PatientCSVFileReader } and the list of patients will be added to
     * patientRegister
     *
     * Also shows the user a message with success or failure after attempted import.
     */
    @FXML
    public void importFromCSV() {
        FileChooser fileChooser = new FileChooser();
        File importFile = fileChooser.showOpenDialog(patientTableView.getScene().getWindow());

        if(importFile == null) {
            return;
        }

        try {
            int patientsBeforeImport = patientRegister.getNumberOfPatients();

            PatientCSVFileReader csvFileReader = new PatientCSVFileReader(importFile.getAbsolutePath());

            patientRegister.addPatients(csvFileReader.getFileDataAsPatientObjects());

            int numberOfPatientsImported = patientRegister.getNumberOfPatients() - patientsBeforeImport;
            InformationDialog informationDialog = new InformationDialog("Successfully imported data for " + numberOfPatientsImported + " patients from " + importFile.getAbsolutePath());
            informationDialog.show();

            setStatus(numberOfPatientsImported + " new patients added.");

        } catch (IOException | IllegalArgumentException | IllegalCSVFormatException e) {
            ErrorDialog err = new ErrorDialog(e.getMessage());
            e.printStackTrace();
            err.showAndWait();
            setStatus("CSV-Import failed");
            return;
        } catch (PatientAlreadyExistsException e) {
            ErrorDialog err = new ErrorDialog(e.getMessage());
            err.showAndWait();
            setStatus("Last CSV-Import partially successful");
        } catch (IllegalFileFormatException e) {
            ErrorDialog err = new ErrorDialog("The chosen file type is not valid, must be a .csv file. Please choose another file by clicking on Select File or cancel the operation");
            err.showAndWait();
            importFromCSV();
        } catch (DatabaseConnectionException e) {
            ErrorDialog errorDialog = new ErrorDialog("Unable to connect to database. Reason: " + e.getMessage());
            errorDialog.showAndWait();
            e.printStackTrace();
        }

        updatePatientTableView();
    }

    /**
     * Triggered by fxml-document.
     * Triggers export to CSV.
     *
     * Opens a save file dialog for the user. Where the user can select save-location and file-name.
     * The selected file-path will be used to create a instance of {@link PatientCSVFileWriter }, which will be used to
     * write the CSV-file with patient-data from patientRegister.
     *
     * Also shows the user a message with success or failure after attempted export.
     */
    @FXML
    public void exportToCSV() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialFileName("patient-list.csv");
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("csv", "csv"));

        String filePath = fileChooser.showSaveDialog(patientTableView.getScene().getWindow()).getAbsolutePath();

        if(FileUtils.fileExists(filePath)) {
            ConfirmDialog confirmDelete = new ConfirmDialog(filePath + " already exists. Do you want to overwrite the existing file? All data in current file will be lost!");
            confirmDelete.showAndWait();

            ButtonType result = confirmDelete.getResult();

            if(result == ButtonType.OK) {
                FileUtils.deleteFile(filePath);
            } else {
                return;
            }
        }

        if(filePath == null) {
            //User pressed cancel or exit
            return;
        }

        try {
            PatientCSVFileWriter fileWriter = new PatientCSVFileWriter(filePath);

            fileWriter.saveToFile((ArrayList<Patient>) patientRegister.getAllPatients());

            InformationDialog informationDialog = new InformationDialog("Successfully exported data for " + patientRegister.getNumberOfPatients() + " patients to " + filePath);
            informationDialog.show();

            setStatus("CSV-Export successful");

        } catch (IOException e) {
            setStatus("CSV-Export failed");
            e.printStackTrace();
        }
    }

    /**
     * Triggered by action in fxml-document.
     * Triggers add new patient dialog.
     *
     * Opens a add new patient dialog for the user. The new patient is then added to patientRegister
     */
    @FXML
    public void addNewPatient() {
        PatientDialog createNewPatientDialog = new PatientDialog();
        createNewPatientDialog.showAndWait();

        Patient newPatient = createNewPatientDialog.getResult();

        if(newPatient == null) {
            //User canceled the dialog
            return;
        } else {
            //Adds patient to the register, and updates the table
            try {
                patientRegister.addPatient(newPatient);
                updatePatientTableView();

                setStatus("1 New patient added");
            } catch (PatientAlreadyExistsException e) {
                ErrorDialog errorDialog = new ErrorDialog("A patient with social security number " + newPatient.getSocialSecurityNumber() + " already exists in the patient register");
                errorDialog.showAndWait();
            } catch (DatabaseConnectionException e) {
                ErrorDialog errorDialog = new ErrorDialog("Unable to connect to database. Reason: " + e.getMessage());
                errorDialog.showAndWait();
            }

        }
    }

    /**
     * Triggered by action in fxml-document.
     * Triggers edit patient dialog.
     *
     * Opens a edit patient dialog for the user. The new patient is then added to patientRegister
     */
    @FXML
    public void editSelectedPatient() {
        Patient currentPatient = this.getCurrentlySelectedPatient();

        Patient result = null;

        try {
            PatientDialog patientDialog = new PatientDialog(PatientDialog.Mode.EDIT_PATIENT, currentPatient);
            patientDialog.showAndWait();
            result = patientDialog.getResult();

            patientRegister.updatePatientChanges(result);
        } catch (NullPointerException e) {
            ErrorDialog errorDialog = new ErrorDialog("Please select a patient from the tableview");
            errorDialog.showAndWait();
        } catch (UnknownPatientException e) {
            ErrorDialog errorDialog = new ErrorDialog("Unable to update patient data. Patient not found in Patient Register");
            errorDialog.showAndWait();
        } catch (DatabaseConnectionException e) {
            ErrorDialog errorDialog = new ErrorDialog("Unable to connect to database. Reason: " + e.getMessage());
            errorDialog.showAndWait();
            e.printStackTrace();
        }

        if(result != null) {
            updatePatientTableView();
            setStatus("Edit changes saved successfully");
        }

    }

    /**
     * Triggered by action in fxml-document.
     * Triggers remove of selected patient.
     *
     * Attempts to remove the currently selected patient in the table-view.
     * Opens a confirm dialog for the user. The new patient is then removed from patientRegister if the
     * user confirms.
     */
    @FXML
    public void removeSelectedPatient() {
        Patient patientToBeDeleted = this.getCurrentlySelectedPatient();

        if(patientToBeDeleted == null) {
            ErrorDialog errorDialog = new ErrorDialog("No patient selected. Please select patient to be deleted from the tableview");
            errorDialog.showAndWait();
            return;
        }

        ConfirmDialog confirmRemoval = new ConfirmDialog("Are you certain you would like to remove " + patientToBeDeleted.getFullName() + ", " + patientToBeDeleted.getSocialSecurityNumber());
        confirmRemoval.showAndWait();

        ButtonType result = confirmRemoval.getResult();

        if (result.equals(ButtonType.OK)) {
            try {
                patientRegister.removePatient(patientToBeDeleted);
                setStatus("1 Patient removed");
            } catch (UnknownPatientException e) {
                e.printStackTrace();
                ErrorDialog err = new ErrorDialog(e.getMessage());
                err.show();
                setStatus("Failed removing patient, patient not found in register.");
            } catch (DatabaseConnectionException e) {
                ErrorDialog errorDialog = new ErrorDialog("Unable to connect to database. Reason: " + e.getMessage());
                errorDialog.showAndWait();
                e.printStackTrace();
            }

            updatePatientTableView();
        } else {
            return;
        }

    }

    /**
     * Triggered by action in fxml-document.
     * Opens the about the application dialog.
     */
    @FXML
    public void showAbout() {
        AboutDialog aboutDialog = new AboutDialog();
        aboutDialog.showAndWait();
    }

    /**
     * Triggered by action in fxml-document.
     * Exits the application, with exit-code 0
     */
    @FXML
    public void exitApplication() {
        System.exit(0);
    }


    /**
     * Clears the table, retrives patient list from patientRegister.
     */
    private void updatePatientTableView() {
        //TODO Endre denne
        patientTableView.getItems().clear();
        patientTableView.getItems().addAll(patientRegister.getAllPatients());
    }

    /**
     * Updates status in the status bar.
     * @param s Status to be shown
     */
    private void setStatus(String s) {
        statusText.setText("Status: " + s);
    }

    /**
     * Selects the patient currently selected in the tableview, and returns it.
     * @return The patient currently selected in the tableview
     */
    private Patient getCurrentlySelectedPatient() {
        return patientTableView.getSelectionModel().getSelectedItem();
    }

}
