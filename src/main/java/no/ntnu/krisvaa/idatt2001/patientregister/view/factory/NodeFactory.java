package no.ntnu.krisvaa.idatt2001.patientregister.view.factory;

import javafx.scene.Node;
import no.ntnu.krisvaa.idatt2001.patientregister.view.factory.items.PatientRegisterMenuBar;
import no.ntnu.krisvaa.idatt2001.patientregister.view.factory.items.PatientRegisterToolBar;
import no.ntnu.krisvaa.idatt2001.patientregister.view.factory.items.PatientTableView;
import no.ntnu.krisvaa.idatt2001.patientregister.view.factory.items.StatusBar;

/**
 * Factory class for nodes to the PatientRegister main frame
 */
public class NodeFactory {

    /**
     * Generates node assosiated with given nodetype, and returns this.
     *
     * @param type Type of node, from {@link NodeType}
     * @return The node assosiacted with given type, null if the type is unknown.
     */
    public static Node getNode(NodeType type) {
        Node node = null;

        switch (type) {
            case PATIENT_TABLE_VIEW -> node = new PatientTableView();
            case PATIENT_REGISTER_MENU_BAR -> node = new PatientRegisterMenuBar();
            case PATIENT_REGISTER_TOOL_BAR -> node = new PatientRegisterToolBar();
            case STATUS_BAR -> node = new StatusBar();
        }

        return node;
    }

}
