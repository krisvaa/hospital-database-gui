package no.ntnu.krisvaa.idatt2001.patientregister.exceptions;

/**
 * Exception for when a patient object already exists.
 */
public class PatientAlreadyExistsException extends Exception {

    public PatientAlreadyExistsException(String errorMessage) {
        super(errorMessage);
    }

}
