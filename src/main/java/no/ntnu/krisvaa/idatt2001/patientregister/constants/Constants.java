package no.ntnu.krisvaa.idatt2001.patientregister.constants;

import java.util.HashMap;

/**
 * Contains static fields with different constants for the applications
 */

public class Constants {

    public static final HashMap<String, PatientField> CSV_PATIENT_FIELD_HEADERS = new HashMap<>();
    static {
        CSV_PATIENT_FIELD_HEADERS.put("firstName", PatientField.FIRST_NAME);
        CSV_PATIENT_FIELD_HEADERS.put("lastName", PatientField.SURNAME);
        CSV_PATIENT_FIELD_HEADERS.put("socialSecurityNumber", PatientField.SOCIAL_SECURITY_NUMBER);
        CSV_PATIENT_FIELD_HEADERS.put("generalPractitioner", PatientField.GENERAL_PRACTITIONER);
        CSV_PATIENT_FIELD_HEADERS.put( "diagnosis", PatientField.DIAGNOSIS);
    }

    public static final String CSV_DELIMITER_STRING = ";";

    public static final String PERSISTENCE_UNIT_PROD = "patient-pu";

}
