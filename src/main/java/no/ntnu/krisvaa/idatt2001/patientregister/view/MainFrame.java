package no.ntnu.krisvaa.idatt2001.patientregister.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainFrame extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Loads scenes/main-frame.fxml and adds it to the main window.
     *
     * @param stage Provided by JavaFX
     * @throws IOException If the fxml-file was not found.
     */
    @Override
    public void start(Stage stage) throws IOException {
        Parent main = FXMLLoader.load(getClass().getResource("scenes/main-frame.fxml"));
        Scene scene = new Scene(main);

        stage.setScene(scene);
        stage.show();

        stage.setTitle("Patient Register - SNAPSHOT v1.0");
        stage.sizeToScene();

    }
}
