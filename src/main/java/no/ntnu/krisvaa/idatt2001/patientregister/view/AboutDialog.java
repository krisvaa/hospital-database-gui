package no.ntnu.krisvaa.idatt2001.patientregister.view;

import javafx.scene.control.Alert;

/**
 * Dialog to show information about the application.
 */
public class AboutDialog extends Alert {

    public static final String ABOUT_DIALOG_TITLE = "Patient Register - About";

    public AboutDialog() {
        super(AlertType.INFORMATION);
        setTitle(ABOUT_DIALOG_TITLE);
        setHeaderText("Patient Register\nv 0.1 - SNAPSHOT");
        setContentText("Warning: contains confidential patient information.\n\n" +
                "Created by\n" +
                "Kristian V. Aars");
    }
}
