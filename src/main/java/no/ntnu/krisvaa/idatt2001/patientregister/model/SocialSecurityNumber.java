package no.ntnu.krisvaa.idatt2001.patientregister.model;

import java.util.Objects;

/**
 * Immutable class for representing a Social Security Number
 */
public class SocialSecurityNumber {

    private final String socialSecurityNumber;

    /**
     * Creates an instance of a social security number
     *
     * @param socialSecurityNumber A string of a social security number, without spaces. Must be 11 digits only.
     * @throws IllegalArgumentException If the social security number is not 11 digits, or does not contain numbers only.
     */
    public SocialSecurityNumber(String socialSecurityNumber) throws IllegalArgumentException {
        boolean verified = verifySocialSecurityNumber(socialSecurityNumber);

        if(!verified) {
            throw new IllegalArgumentException(socialSecurityNumber + " is not a valid social security number. Social security number must contain 11 digits.");
        } else {
            this.socialSecurityNumber = socialSecurityNumber;
        }
    }

    /**
     * Method to verify that a social security number is valid (Number with 11 digits)
     * @param s Social security number as a String to be verified
     * @return true if the SSN is valid, false if it is not valid.
     */
    public static boolean verifySocialSecurityNumber(String s) {
        //Verify that the String is eleven charachers long
        if(s == null || s.length() != 11) {
            return false;
        }

        //Verify that the string can be parsed as a number
        try {
            Long.parseLong(s);
        } catch (NumberFormatException e) {
            return false;
        }

         return true;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SocialSecurityNumber that = (SocialSecurityNumber) o;
        return Objects.equals(socialSecurityNumber, that.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "SocialSecurityNumber{" +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }
}
