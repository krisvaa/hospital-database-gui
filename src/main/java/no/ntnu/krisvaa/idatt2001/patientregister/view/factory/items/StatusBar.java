package no.ntnu.krisvaa.idatt2001.patientregister.view.factory.items;

import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class StatusBar extends VBox {

    private Text statusText;

    public StatusBar() {
        statusText = new Text();
        this.getChildren().add(statusText);
        setStatus("OK");

        this.setPrefHeight(20);
        this.setMaxHeight(20);
        this.setMinHeight(20);
    }

    public void setStatus(String s) {
        statusText.setText("Status: " + s);
    }

}
