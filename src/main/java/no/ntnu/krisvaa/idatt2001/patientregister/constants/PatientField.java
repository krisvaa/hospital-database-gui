package no.ntnu.krisvaa.idatt2001.patientregister.constants;

/**
 * List of the different fields that a patient contains.
 */
public enum PatientField {
    FIRST_NAME, SURNAME, SOCIAL_SECURITY_NUMBER, GENERAL_PRACTITIONER, DIAGNOSIS

}
