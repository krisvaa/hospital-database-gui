package no.ntnu.krisvaa.idatt2001.patientregister.exceptions;

/**
 * Exception for database connectivity related errors.
 */
public class DatabaseConnectionException extends Exception {

    public DatabaseConnectionException(String errorMessage) {

    }
}
