package no.ntnu.krisvaa.idatt2001.patientregister.view;

import javafx.scene.control.Alert;

/**
 * Dialog to show the user a error message.
 *
 */
public class ErrorDialog extends Alert {

    private static final String ERROR_DIALOG_TITLE = "Patient Register - Error";

    public ErrorDialog(String message) {
        super(AlertType.ERROR);
        this.setTitle(ERROR_DIALOG_TITLE);
        this.setContentText(message);

    }
}
