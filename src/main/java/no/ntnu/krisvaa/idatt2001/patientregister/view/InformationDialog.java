package no.ntnu.krisvaa.idatt2001.patientregister.view;

import javafx.scene.control.Alert;

/**
 * Information dialog to show the user an information message
 */
public class InformationDialog extends Alert {

    private static final String INFORMATION_DIALOG_TITLE = "Patient Register - Info";

    public InformationDialog(String message) {
        super(Alert.AlertType.INFORMATION);
        this.setTitle(INFORMATION_DIALOG_TITLE);
        this.setContentText(message);
        this.setHeaderText("");
    }
}
