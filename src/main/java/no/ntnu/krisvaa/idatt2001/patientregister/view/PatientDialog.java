package no.ntnu.krisvaa.idatt2001.patientregister.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import no.ntnu.krisvaa.idatt2001.patientregister.model.Patient;

public class PatientDialog extends Dialog<Patient> {

    /**
     * Different modes of the PatientDialog.
     *
     */
    public enum Mode {
        NEW_PATIENT, EDIT_PATIENT, VIEW_PATIENT
    }

    private Label dialogTitle;
    private TextField firstNameTextField;
    private TextField surnameTextField;
    private TextField socialSecurityNumberTextField;

    private final Mode mode;
    private Patient existingPatient;

    /**
     * Default constructor, creates a "Add new patient" dialog.
     */
    public PatientDialog() {
        this(Mode.NEW_PATIENT, null);
    }


    /**
     * Creates a Patient Dialog with text-fields for First name, surname and Social security number
     *
     * @param mode Mode of the dialog, from {@link Mode}
     * @param existingPatient Patient to be edited, or viewed. Can be null if a new patient shall be created
     * @throws NullPointerException If no patient was provided, when it is necessary.
     */
    public PatientDialog(Mode mode, Patient existingPatient) throws NullPointerException {
        this.mode = mode;
        this.existingPatient = existingPatient;

        VBox main = null;

        try {
            main = FXMLLoader.load(getClass().getResource("scenes/patient-dialog.fxml"));

        } catch (Exception e) {
            e.printStackTrace();
            ErrorDialog err = new ErrorDialog("Unable to load dialog-frame for Patient due to missing application critical files.");
            err.showAndWait();
        }

        this.getDialogPane().setContent(main);
        this.getDialogPane().setMaxHeight(250);
        this.setWidth(450);
        this.setHeight(250);
        main.setMaxHeight(250);

        //Retrives the nodes by Style-ID
        dialogTitle = (Label) this.getDialogPane().lookup("#dialogTitle");
        firstNameTextField = (TextField) this.getDialogPane().lookup("#firstNameTextField");
        surnameTextField = (TextField) this.getDialogPane().lookup("#surnameTextField");
        socialSecurityNumberTextField = (TextField) this.getDialogPane().lookup("#socialSecurityNumberTextField");

        //Initialized the correct mode
        switch (mode) {
            case EDIT_PATIENT -> initializeEdit();
            case NEW_PATIENT -> initializeNewPatient();
            default -> initializeViewPatient();
        }

        //Adds OK and Cancel buttons
        getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        getDialogPane().getButtonTypes().add(ButtonType.OK);

        //Saves changes to patient, and returns the patient object as the result of the dialog-box
        setResultConverter((buttonType) -> {
            if(buttonType == ButtonType.OK) {
                saveChanges();
                return this.existingPatient;
            } else {
                //When pressing cancel or Exit, changes should not be saved
                return null;
            }
        });
    }

    /**
     * Initializes all fields as disabled, and therefore view-only. Also fills the text-fields with patient data
     * @throws NullPointerException
     */
    private void initializeViewPatient() throws NullPointerException {
        initializeEdit();

        dialogTitle.setText("View Patient");

        firstNameTextField.setDisable(true);
        surnameTextField.setDisable(true);
        socialSecurityNumberTextField.setDisable(true);
    }

    /**
     * Fills text-fields with existing data from the patient.
     * Sets social security number as disabled, to prevent editing.
     *
     * @throws NullPointerException
     */
    private void initializeEdit() throws NullPointerException {
        if(existingPatient == null) {
            throw new NullPointerException("No patient provided for edit-dialog");
        }

        dialogTitle.setText("Edit Patient");
        firstNameTextField.setText(existingPatient.getFirstName());
        surnameTextField.setText(existingPatient.getSurname());
        socialSecurityNumberTextField.setText(existingPatient.getSocialSecurityNumber());

        socialSecurityNumberTextField.setDisable(true);
    }

    /**
     * Sets title as "Add new patient" and leaves all text-fields open for edit.
     */
    private void initializeNewPatient() {
        dialogTitle.setText("Add new patient");
    }

    /**
     * Saves the changes to the patient object. Or creates a new Patient object from the values provided if a new
     * patient shall be created.
     *
     * @throws IllegalArgumentException If any patient-values are invalid.
     */
    private void saveChanges() throws IllegalArgumentException {
        String firstName = firstNameTextField.getText();
        String surname = surnameTextField.getText();
        String socialSecurityNumber = socialSecurityNumberTextField.getText();

        switch (mode) {
            case EDIT_PATIENT -> {
                try {
                    existingPatient.setFirstName(firstName);
                    existingPatient.setSurname(surname);

                } catch (IllegalArgumentException e) {
                    ErrorDialog errDialog = new ErrorDialog(e.getMessage());
                    errDialog.show();
                    throw e;
                }
            }

            case NEW_PATIENT -> {
                try {
                    existingPatient = new Patient(firstName, surname, socialSecurityNumber);
                } catch (IllegalArgumentException e) {
                    ErrorDialog errorDialog = new ErrorDialog(e.getMessage());
                    errorDialog.show();
                    throw e;
                }
            }

        }

        setResult(existingPatient);
    }

}
