package no.ntnu.krisvaa.idatt2001.patientregister.exceptions;

/**
 * Exception for when a given patient object is not found.
 */
public class UnknownPatientException extends Exception {

    public UnknownPatientException(String errorMessage) {
        super(errorMessage);
    }
}
