package no.ntnu.krisvaa.idatt2001.patientregister.model;

import no.ntnu.krisvaa.idatt2001.patientregister.constants.Constants;
import no.ntnu.krisvaa.idatt2001.patientregister.constants.PatientField;
import no.ntnu.krisvaa.idatt2001.patientregister.exceptions.IllegalCSVFormatException;
import no.ntnu.krisvaa.idatt2001.patientregister.exceptions.IllegalFileFormatException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Class used to generate Patient objects from a CSV-File
 */
public class PatientCSVFileReader {

    private final File FILE;
    private String delimiter = Constants.CSV_DELIMITER_STRING;

    /**
     * Creates an instance of CSVFileReader. Only .csv is allowed.
     *
     * @param path Full path to file on disk
     * @throws IOException If the file does not exist, or there is error reading the file.
     * @throws IllegalFileFormatException If the file is not .csv
     */
    public PatientCSVFileReader(String path) throws IOException, IllegalFileFormatException {
        if(!path.endsWith(".csv")) {
            throw new IllegalFileFormatException(path + " is not a .csv file.");
        }

        this.FILE = new File(path);

        if(!FILE.exists()) {
            throw new IOException("Could not find the file " + FILE.getAbsolutePath());
        }
    }

    /**
     * Reads data from {@link #FILE}, splits the data into lines. And splits each line into values where delimiter ; is found
     *
     * @return List containing lines from CSV file. And one line is represented a list of String values.
     * @throws IOException Problems reading from the file.
     */
    private ArrayList<ArrayList<String>> getRawFileDataFromFile() throws IOException {
        ArrayList<ArrayList<String>> fileData = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(FILE))){
            String line;
            while((line = bufferedReader.readLine()) != null){
                //Splits each line by the given delimiter into an array of Strings
                ArrayList<String> values = new ArrayList<>(Arrays.asList(line.split("\\"+delimiter)));
                fileData.add(values);
            }
        }

        return fileData;
    }

    /**
     * Method to generate, and return {@link Patient} objects from the CSV-File provided in constructor.
     *
     * Uses raw data to read out the first line as header values, and maps their indexes according to {@link Constants#CSV_PATIENT_FIELD_HEADERS}
     * The method then reads out the rest of the lines, and maps the correct values to a new {@link Patient} object by using the header index generated earlier.
     *
     * The new objects are added to an ArrayList and are returned.
     *
     * @return ArrayList with {@link Patient} objects retrieved from CSV-File.
     * @throws IOException If there is error reading the CSV-File
     * @throws IllegalCSVFormatException If the format of the CSV-File is incorrect.
     * @throws IllegalArgumentException If any of the values are invalid.
     */
    public ArrayList<Patient> getFileDataAsPatientObjects() throws IOException, IllegalCSVFormatException, IllegalArgumentException {
        ArrayList<ArrayList<String>> rawData = getRawFileDataFromFile();
        ArrayList<String> headerData = rawData.remove(0);
        HashMap<PatientField, Integer> headerIndex = new HashMap<>();
        ArrayList<Patient> patientList = new ArrayList<>();


        for(int i = 0; i < headerData.size(); i++) {
            String header = headerData.get(i);
            PatientField field = Constants.CSV_PATIENT_FIELD_HEADERS.get(header);

            headerIndex.put(field, i);
        }

        if(headerIndex.size() != headerData.size()) {
            throw new IllegalCSVFormatException("CSV header format is incorrect");
        }

        for(ArrayList<String> line : rawData) {
            //Required
            String firstName = line.get(headerIndex.get(PatientField.FIRST_NAME));
            String surname = line.get(headerIndex.get(PatientField.SURNAME));
            String socialSecurityNumber = line.get(headerIndex.get(PatientField.SOCIAL_SECURITY_NUMBER));
            String diagnosis = null;
            String generalPractitioner = null;

            //Optional
            try {
                generalPractitioner = line.get(headerIndex.get(PatientField.GENERAL_PRACTITIONER));
                diagnosis = line.get(headerIndex.get(PatientField.DIAGNOSIS));
            } catch (Exception e) {

            }

            try {
                Patient patient = new Patient(firstName, surname, socialSecurityNumber);
                patient.setGeneralPractitioner(generalPractitioner);
                patient.setDiagnosis(diagnosis);

                patientList.add(patient);
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("Error at line " + (rawData.indexOf(line)+2) + ": " + e.getMessage());
            }

        }

        return patientList;
    }
}
