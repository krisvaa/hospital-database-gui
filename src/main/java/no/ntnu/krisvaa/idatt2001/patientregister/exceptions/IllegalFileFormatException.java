package no.ntnu.krisvaa.idatt2001.patientregister.exceptions;

/**
 * Exception for when the file-ending of a file is not as intended.
 */
public class IllegalFileFormatException extends Exception {

    public IllegalFileFormatException(String message) {
        super(message);
    }
}
