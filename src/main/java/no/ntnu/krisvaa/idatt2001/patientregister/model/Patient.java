package no.ntnu.krisvaa.idatt2001.patientregister.model;



import javax.persistence.Entity;
import javax.persistence.Id;

import java.util.Objects;

/**
 * Class to represent a patient
 */
@Entity
public class Patient {

    @Id
    private String socialSecurityNumber;
    private String firstName;
    private String surname;
    private String diagnosis;
    private String generalPractitioner;

    public Patient() {

    }

    /**
     * Creates a patient with a blank diagnosis and no general practitioner.
     *
     * @param socialSecurityNumber Social security number of patient (Must be a digit with length of eleven)
     * @param firstName First name of the patient, cannot be null or empty
     * @param surname Last name of the patient, cannot be null or empty
     *
     * @throws IllegalArgumentException If name name values, or social security number is not valid.
     */
    public Patient(String firstName, String surname, String socialSecurityNumber) throws IllegalArgumentException {
        if(!SocialSecurityNumber.verifySocialSecurityNumber(socialSecurityNumber)) {
            throw new IllegalArgumentException("Invalid format on social security number");
        }

        this.socialSecurityNumber = socialSecurityNumber;
        setFirstName(firstName);
        setSurname(surname);
    }

    /**
     * @return The social security number as a String
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Functions is deprecatd, currently does nothing.
     *
     * Set the social security number.
     * Format: DDMMYYXXXXX
     *
     * @param socialSecurityNumber A number with eleven digits, in String-format.
     * @throws IllegalArgumentException If the socialSecurityNumber is not a valid format.
     */
    @Deprecated
    public void setSocialSecurityNumber(String socialSecurityNumber) throws IllegalArgumentException {
        //this.socialSecurityNumber = new SocialSecurityNumber(socialSecurityNumber);
    }

    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName First name of the patient
     * @throws IllegalArgumentException If the name is equal to null or an empty String
     */
    public void setFirstName(String firstName) throws IllegalArgumentException {
        IllegalArgumentException exception = new IllegalArgumentException("First name cannot be null, or empty");
        if(firstName == null) {
            throw exception;
        } else if(firstName.equals("")) {
            throw exception;
        }
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    /**
     *
     * @param surname Last name/surname of the patient
     * @throws IllegalArgumentException If the name is equal to null or an empty String
     */
    public void setSurname(String surname) throws IllegalArgumentException {
        IllegalArgumentException exception = new IllegalArgumentException("Surname cannot be null, or empty");
        if(surname == null) {
            throw exception;
        } else if(surname.equals("")) {
            throw exception;
        }
        this.surname = surname;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Returns the firstname and surname in one String
     * @return Firstname and surname, with one space between
     */
    public String getFullName() {
        return firstName + " " + surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return socialSecurityNumber.equals(patient.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber, firstName, surname, diagnosis, generalPractitioner);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber=" + getSocialSecurityNumber() +
                ", firstName='" + getFirstName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                ", diagnosis='" + getDiagnosis() + '\'' +
                ", generalPractitioner='" + getGeneralPractitioner() + '\'' +
                '}';
    }
}
