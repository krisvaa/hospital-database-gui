package no.ntnu.krisvaa.idatt2001.patientregister.view.factory;

/**
 * Node types used in {@link NodeFactory}
 */
public enum NodeType {
    PATIENT_REGISTER_MENU_BAR,
    PATIENT_REGISTER_TOOL_BAR,
    PATIENT_TABLE_VIEW,
    STATUS_BAR
}
