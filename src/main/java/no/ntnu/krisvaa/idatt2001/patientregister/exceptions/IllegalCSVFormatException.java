package no.ntnu.krisvaa.idatt2001.patientregister.exceptions;

/**
 * Exception for when the format of a CSV file is not as intended.
 */
public class IllegalCSVFormatException extends Exception{

    public IllegalCSVFormatException(String errorMessage) {
        super(errorMessage);
    }

}
