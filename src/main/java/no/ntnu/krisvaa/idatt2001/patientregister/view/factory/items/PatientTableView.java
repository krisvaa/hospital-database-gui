package no.ntnu.krisvaa.idatt2001.patientregister.view.factory.items;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import no.ntnu.krisvaa.idatt2001.patientregister.model.Patient;

public class PatientTableView extends TableView<Patient> {

    private TableColumn<Patient, String> firstNameColumn;
    private TableColumn<Patient, String> surnameColumn;
    private TableColumn<Patient, String> socialSecurityNumberColumn;

    public PatientTableView() {
        super();

        firstNameColumn = new TableColumn<>();
        surnameColumn = new TableColumn<>();
        socialSecurityNumberColumn = new TableColumn<>();

        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));

        this.getColumns().add(firstNameColumn);
        this.getColumns().add(surnameColumn);
        this.getColumns().add(socialSecurityNumberColumn);
    }



}
