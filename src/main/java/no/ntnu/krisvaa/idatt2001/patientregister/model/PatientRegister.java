package no.ntnu.krisvaa.idatt2001.patientregister.model;

import no.ntnu.krisvaa.idatt2001.patientregister.exceptions.DatabaseConnectionException;
import no.ntnu.krisvaa.idatt2001.patientregister.exceptions.PatientAlreadyExistsException;
import no.ntnu.krisvaa.idatt2001.patientregister.exceptions.UnknownPatientException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Class to represent a Patient Register
 *
 */
public class PatientRegister {

    private ArrayList<Patient> patients;

    //Entity manager for derby-database
    private EntityManager entityManager;

    /**
     * Creates an instance of PatientRegister.
     * Patient register also creates a EntityManager for communicating with the database.
     * The database connection is based on the persistenceUnitName
     *
     * @param persistenceUnitName Which persistence unit the register should attach to
     * @throws InvocationTargetException If there is an error communicating with the database.
     */
    public PatientRegister(String persistenceUnitName) throws DatabaseConnectionException {
        initializeDatabaseConnection(persistenceUnitName);

        Query q = entityManager.createQuery("select p from Patient p");
        patients = (ArrayList<Patient>) q.getResultList();
    }

    /**
     * Initializes database connection using patient-pu
     */
    private void initializeDatabaseConnection(String persistenceUnitName) throws IllegalStateException, DatabaseConnectionException {
        EntityManagerFactory emFactory = Persistence.createEntityManagerFactory(persistenceUnitName);
        entityManager = emFactory.createEntityManager();
    }

    /**
     * Add a new patient to the patientRegister.
     * @param p Patient to be added
     * @throws PatientAlreadyExistsException If there already exists a patient with identical Social security number
     */
    public void addPatient(Patient p) throws PatientAlreadyExistsException, DatabaseConnectionException {
        if(patientExists(p)) {
            throw new PatientAlreadyExistsException("Patient " + p + " already exists in patient register");
        }

        try {
            entityManager.getTransaction().begin();
            entityManager.persist(p);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            throw new DatabaseConnectionException("Unable to connect to database. Reason: " + e.getMessage());
        }

        patients.add(p);
    }

    /**
     * Uploads changes to patients to the database
     */
    public void updatePatientChanges(Patient p) throws UnknownPatientException, DatabaseConnectionException {
        if(!patientExists(p)) {
            throw new UnknownPatientException("Patient not found in Patient register");
        }

        try {
            entityManager.getTransaction().begin();
            entityManager.merge(p);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            throw new DatabaseConnectionException("Unable to connect to database. Reason: " + e.getMessage());
        }

    }

    /**
     * Add a list of patients to the patientRegister
     *
     * @param patients List of patients to be added.
     * @throws PatientAlreadyExistsException If there already exists a patient(s) with identical Social security number
     */
    public void addPatients(ArrayList<Patient> patients) throws PatientAlreadyExistsException, DatabaseConnectionException {
        ArrayList<Patient> failedToAdd = new ArrayList<>();

        patients.forEach(p -> {
            try {
                this.addPatient(p);
            } catch (PatientAlreadyExistsException | DatabaseConnectionException e) {
                failedToAdd.add(p);
            }
        });

        if(!failedToAdd.isEmpty()) {
            throw new PatientAlreadyExistsException(failedToAdd.size() + " patients were not added to patient register because they already exist. " + failedToAdd.toString());
        }
    }

    /**
     * Returns a copy of the patientlist
     *
     * @return Copy of the patientlist
     */
    public Collection<Patient> getAllPatients() {
        ArrayList<Patient> copy = new ArrayList<>(patients);

        return copy;
    }

    @Override
    public String toString() {
        return "PatientRegister{" +
                "patients=" + patients +
                '}';
    }

    /**
     * Remove patient from the patient register
     *
     * @param patient Patient to be removed
     * @throws UnknownPatientException If the patient is not found in the register
     */
    public void removePatient(Patient patient) throws UnknownPatientException, DatabaseConnectionException {
        boolean success = patients.remove(patient);
        if(!success) {
            throw new UnknownPatientException("Patient " + patient + " was not found in patient register");
        }

        try {
            entityManager.getTransaction().begin();
            entityManager.remove(patient);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            throw new DatabaseConnectionException("Unable to connect to database. Reason: " + e.getMessage());
        }

    }

    public int getNumberOfPatients() {
        return patients.size();
    }

    public boolean patientExists(Patient p) {
        return patients.contains(p);
    }
}
