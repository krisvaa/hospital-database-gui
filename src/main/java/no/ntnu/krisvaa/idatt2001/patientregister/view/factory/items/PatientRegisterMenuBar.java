package no.ntnu.krisvaa.idatt2001.patientregister.view.factory.items;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

public class PatientRegisterMenuBar extends MenuBar {

    private Menu fileMenu;
    private Menu edit;
    private Menu help;

    public PatientRegisterMenuBar() {
        fileMenu = new Menu();
        edit = new Menu();
        help = new Menu();

        fileMenu.getItems().addAll(
                new MenuItem("Import from .CSV"),
                new MenuItem("Export to .CSV"),
                new SeparatorMenuItem(),
                new MenuItem("Exit")
        );

        edit.getItems().addAll(
                new MenuItem("Add new patient"),
                new MenuItem("Edit selected patient"),
                new MenuItem("Remove selected patient")
        );

        help.getItems().addAll(
                new MenuItem("About")
        );

        this.getMenus().addAll(
                fileMenu,
                edit,
                help
        );
    }
}
