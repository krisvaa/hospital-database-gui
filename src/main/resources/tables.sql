CREATE TABLE IF NOT EXISTS Patient (
    socialSecurityNumber VARCHAR(11) NOT NULL,
    firstName VARCHAR(256) NULL,
    surname VARCHAR(256),
    diagnosis VARCHAR(256),
    generalPractitioner VARCHAR(256),
  PRIMARY KEY (socialSecurityNumber));

