package no.ntnu.krisvaa.idatt2001.patientregister.model;

import no.ntnu.krisvaa.idatt2001.patientregister.exceptions.IllegalFileFormatException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class PatientCSVFileTest {

    private static ArrayList<Patient> patients;

    private static PatientCSVFileWriter csvFileWriter;

    private static final String TEST_PATIENTS_FILE = "src/test/resources/test-patients.csv";
    private static final String TEST_EXPORT_PATIENTS_FILE = "src/test/resources/test-patients-export.csv";

    @BeforeAll
    public static void setUp() throws IOException {
        patients = new ArrayList<>();

        try {
            csvFileWriter = new PatientCSVFileWriter(TEST_EXPORT_PATIENTS_FILE);
        } catch (IOException e) {
            throw e;
        }

    }

    @Order(1)
    @Test
    public void readFromFile() {
        try {
            PatientCSVFileReader csvFileReader = new PatientCSVFileReader(TEST_PATIENTS_FILE);

            assertDoesNotThrow(() -> {
                patients = csvFileReader.getFileDataAsPatientObjects();}
            );
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalFileFormatException e) {
            e.printStackTrace();
        }


        ArrayList<Patient> expectedList = new ArrayList<>();
        expectedList.add(new Patient("Andrø", "Tanker", "29104300764"));
        expectedList.add(new Patient("Ane", "Rikke", "12073004096"));
        expectedList.add(new Patient("Anna Ellen","Flom","14109605264"));
        expectedList.add(new Patient("Anne","Ledes","28033418908"));

        expectedList.get(0).setGeneralPractitioner("Maren G. Skake");
        expectedList.get(1).setGeneralPractitioner("Dr. Klø");
        expectedList.get(2).setGeneralPractitioner("Matti Ompa");
        expectedList.get(3).setGeneralPractitioner("Rally McBil");

        expectedList.get(2).setDiagnosis("covid19");

        assertPatientListEquals(expectedList, patients);

    }

    @Order(2)
    @Test
    public void saveToFile() {
        readFromFile();

        //Removes Andrø Tanker
        patients.remove(0);

        //Write to file
        try {
            csvFileWriter.saveToFile(patients);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<Patient> expectedList = patients;

        //Try to read from file
        try {
            PatientCSVFileReader csvFileReader = new PatientCSVFileReader(TEST_EXPORT_PATIENTS_FILE);

            assertDoesNotThrow(() -> {
                patients = csvFileReader.getFileDataAsPatientObjects();}
            );
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalFileFormatException e) {
            e.printStackTrace();
        }

        assertPatientListEquals(expectedList, patients);
    }

    public void assertPatientListEquals(ArrayList<Patient> expected, ArrayList<Patient> actual) {

        for(Patient patient : actual) {
            Patient expectedPatient = expected.stream().filter(p -> patient.equals(p)).findFirst().get();

            assertNotNull(expectedPatient);

            assertEquals(expectedPatient.getSocialSecurityNumber(), patient.getSocialSecurityNumber());
            assertEquals(expectedPatient.getFirstName(), patient.getFirstName());
            assertEquals(expectedPatient.getSurname(), patient.getSurname());
            assertEquals(expectedPatient.getDiagnosis(), patient.getDiagnosis());
            assertEquals(expectedPatient.getGeneralPractitioner(), patient.getGeneralPractitioner());
        }

    }

    @AfterAll
    public static void cleanUp() {
        File exportFile = new File(TEST_EXPORT_PATIENTS_FILE);
        if(exportFile.exists()) {
            exportFile.delete();
        }
    }
}