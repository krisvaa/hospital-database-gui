package no.ntnu.krisvaa.idatt2001.patientregister.model;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {

    private static Patient testPatient;

    private static final String invalidSSN = "1234567890";
    private static final String validSSN = "12345678901";

    @BeforeEach
    public void setUp() {
        assertThrows(IllegalArgumentException.class, () -> {
            testPatient = new Patient("Ola", "Normann", invalidSSN);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            testPatient = new Patient("", "Normann", invalidSSN);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            testPatient = new Patient("Ola", "", invalidSSN);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            testPatient = new Patient(null, "Normann", invalidSSN);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            testPatient = new Patient("Ola", null, invalidSSN);
        });

        testPatient = new Patient("Ola", "Normann", validSSN);
    }

    @Test
    public void setFirstName() {
        assertThrows(IllegalArgumentException.class, () -> {
            testPatient.setFirstName("");
        });

        testPatient.setFirstName("Oline");

        assertEquals("Oline", testPatient.getFirstName());
    }

    @Test
    public void setSurname() {
        assertThrows(IllegalArgumentException.class, () -> {
            testPatient.setSurname("");
        });

        testPatient.setSurname("Halvorsen");

        assertEquals("Halvorsen", testPatient.getSurname());
    }

    @Test
    public void setDiagnosis() {
        testPatient.setDiagnosis("");
        assertEquals("", testPatient.getDiagnosis());

        testPatient.setDiagnosis("Covid-19 virus");

        assertEquals("Covid-19 virus", testPatient.getDiagnosis());
    }

    @Test
    public void setGeneralPractitioner() {
        testPatient.setGeneralPractitioner("");
        assertEquals("", testPatient.getGeneralPractitioner());

        testPatient.setGeneralPractitioner("Dr. Petter Aarseth");

        assertEquals("Dr. Petter Aarseth", testPatient.getGeneralPractitioner());
    }

    @Test
    public void testEquals() {
        Patient p = new Patient("Ola", "Normann", validSSN);

        assertTrue(p.equals(testPatient));

        Patient p2 = new Patient("Ola", "Normann", "12345678907");

        assertFalse(p2.equals(testPatient));
    }

    @Test
    public void getFullName() {
        Patient p = new Patient("Ola", "Normann", validSSN);

        assertEquals("Ola Normann", p.getFullName());
    }

}