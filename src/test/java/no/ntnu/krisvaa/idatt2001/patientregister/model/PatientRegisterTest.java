package no.ntnu.krisvaa.idatt2001.patientregister.model;

import no.ntnu.krisvaa.idatt2001.patientregister.constants.Constants;
import no.ntnu.krisvaa.idatt2001.patientregister.exceptions.DatabaseConnectionException;
import no.ntnu.krisvaa.idatt2001.patientregister.exceptions.PatientAlreadyExistsException;
import no.ntnu.krisvaa.idatt2001.patientregister.exceptions.UnknownPatientException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterTest {

    private static PatientRegister patientRegister;
    private static int numberOfPatients = 0;

    @BeforeAll
    public  static void setUp() throws DatabaseConnectionException {
        //There should be a seperate PERSISTENCE_UNIT_TEST, but due to limited time this has not been added.

        patientRegister = new PatientRegister(Constants.PERSISTENCE_UNIT_PROD);

        numberOfPatients = patientRegister.getNumberOfPatients();
    }

    @Order(1)
    @Test
    public void addPatient() {
        Patient p = new Patient("Ola", "Normann", "12345678900");

        assertDoesNotThrow(() -> {
            patientRegister.addPatient(p);
        });

        assertTrue(patientRegister.patientExists(p));

        numberOfPatients += 1;

        Patient p2 = new Patient("Kasper", "Normann", "12345678900");

        assertThrows(PatientAlreadyExistsException.class, () -> {
            //Tries to add patient with identical SSN twice
           patientRegister.addPatient(p);
        });
    }

    @Order(2)
    @Test
    public void addPatients() {
        ArrayList<Patient> patients = new ArrayList<>();
        Patient p1 = new Patient("Per", "Askeladd", "12345678901");
        Patient p2 = new Patient("Pål", "Askeladd", "12345678902");
        Patient p3 = new Patient("Espen", "Askeladd", "12345678903");
        patients.add(p1);
        patients.add(p2);
        patients.add(p3);

        assertDoesNotThrow(() -> {
            patientRegister.addPatients(patients);
        });

        assertTrue(patientRegister.patientExists(p1));
        assertTrue(patientRegister.patientExists(p2));
        assertTrue(patientRegister.patientExists(p3));

        numberOfPatients += 3;

        //Tries to add the same patients twice
        assertThrows(PatientAlreadyExistsException.class, () -> {
            patientRegister.addPatients(patients);
        });
    }

    @Order(3)
    @Test
    public void getAllPatients() {
        //Number of patients in collection should be 4, from the two previous tests
        assertEquals(numberOfPatients, patientRegister.getAllPatients().size());
    }

    @Order(6)
    @Test
    public void removePatient() throws PatientAlreadyExistsException, DatabaseConnectionException {
        Patient p = new Patient("Trine", "Andersen", "12345678906");

        patientRegister.addPatient(p);
        assertDoesNotThrow(() -> {
            patientRegister.removePatient(p);
        });

        assertFalse(patientRegister.patientExists(p));

        //Tries to remove patient twice, should throw an error.
        assertThrows(UnknownPatientException.class, () -> {
            patientRegister.removePatient(p);
        });
    }

    @Order(4)
    @Test
    public void getNumberOfPatients() {
        //Number of patients in collection should be 4, from the two previous tests
        assertEquals(numberOfPatients, patientRegister.getNumberOfPatients());
    }

    @Order(5)
    @Test
    public void patientExists() throws PatientAlreadyExistsException, DatabaseConnectionException {
        Patient p = new Patient("Chris", "Chrome", "12345678904");
        patientRegister.addPatient(p);

        numberOfPatients += 1;

        assertTrue(patientRegister.patientExists(p));

        Patient p2 = new Patient("Chris", "Explorer", "12345678905");
        assertFalse(patientRegister.patientExists(p2));
    }

    @AfterAll
    public static void cleanUp() {
        try {
            patientRegister.removePatient(patientRegister.getAllPatients().stream().filter(p -> p.getSocialSecurityNumber().equals("12345678904")).findFirst().get());
            patientRegister.removePatient(patientRegister.getAllPatients().stream().filter(p -> p.getSocialSecurityNumber().equals("12345678901")).findFirst().get());
            patientRegister.removePatient(patientRegister.getAllPatients().stream().filter(p -> p.getSocialSecurityNumber().equals("12345678902")).findFirst().get());
            patientRegister.removePatient(patientRegister.getAllPatients().stream().filter(p -> p.getSocialSecurityNumber().equals("12345678903")).findFirst().get());
            patientRegister.removePatient(patientRegister.getAllPatients().stream().filter(p -> p.getSocialSecurityNumber().equals("12345678900")).findFirst().get());
        } catch (UnknownPatientException e) {
            e.printStackTrace();
        } catch (DatabaseConnectionException e) {
            e.printStackTrace();
        }
    }
}