package no.ntnu.krisvaa.idatt2001.patientregister.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SocialSecurityNumberTest {

    @Test
    public void createSocialSecurityNumber() {
        //Should throw IllegalArgumentException due to it containing letter
        assertThrows(IllegalArgumentException.class, () -> {
            SocialSecurityNumber s = new SocialSecurityNumber("1111965678A");
        });

        //Should throw IllegalArgumentException due to it being 12 numbers
        assertThrows(IllegalArgumentException.class, () -> {
            SocialSecurityNumber s = new SocialSecurityNumber("111196567809");
        });

        assertDoesNotThrow(() -> {
            SocialSecurityNumber s = new SocialSecurityNumber("11239698765");
        });
    }

    @Test
    public void getSocialSecurityNumber() {
        SocialSecurityNumber s = new SocialSecurityNumber("11239698765");
        assertEquals("11239698765", s.getSocialSecurityNumber());
    }

    @Test
    public void testEquals() {
        SocialSecurityNumber s1 = new SocialSecurityNumber("11239698765");
        SocialSecurityNumber s2 = new SocialSecurityNumber("11239698765");

        assertTrue(s1.equals(s2));
    }
}